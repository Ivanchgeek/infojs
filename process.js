function process_gist() {
    var gists = document.getElementsByClassName("gistagramm");
    for (var i = 0; i < gists.length; i++) {
        var values = gists[i].dataset.vals.split(";");
        var colours = [];
        if (gists[i].dataset.colours != undefined) colours = gists[i].dataset.colours.split(";");
        var titles = [];
        if (gists[i].dataset.titles != undefined) titles = gists[i].dataset.titles.split(";");
        var show = gists[i].dataset.shownumbers != "false";
        for (var j = 0; j < values.length; j++) {
            var el = document.createElement("div");
            el.classList.add("coloumn");
            el.dataset.val = values[j];
            if (colours.length > 0) el.style.background = colours[j];
            if (titles.length > 0) el.title = titles[j];
            gists[i].appendChild(el);
        }
        var cols = gists[i].getElementsByClassName("coloumn");
        var sum = 0;
        for (var j = 0; j < cols.length; j++) {
            sum += parseInt(cols[j].dataset.val);
        }
        for (var j = 0; j < cols.length; j++) {
            cols[j].style.height = parseInt(cols[j].dataset.val) / sum * 90 + "%";
            cols[j].style.marginTop = parseInt(cols[j].dataset.val) / sum * 90 + "%"; 
            if (show) cols[j].innerHTML = "<p style='margin-top: -20px'>" + cols[j].dataset.val + "</p>";
            if (gists[i].clientWidth / cols.length < gists[i].clientWidth * 0.12) {
                cols[j].style.width = gists[i].clientWidth / (cols.length + 3) + "px"; 
            }
        }
    }
}

window.onload = function() {
    process_gist();
}

scripts = document.getElementsByTagName('head')[0].getElementsByTagName("script");
iam = null;
for (var i = 0; i < scripts.length; i++) {
    if (scripts[i].id == "infojs-script") {
        iam = scripts[i];
    }
}
var css = document.createElement("link");
css.rel = "stylesheet";
css.type = "text/css";
css.href = "demo.css";
document.getElementsByTagName('head')[0].insertBefore(css, iam);